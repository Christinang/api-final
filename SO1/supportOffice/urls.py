from django.conf.urls import url
from . import views
app_name ='supportOffice'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^home/$', views.home, name='home'),
    url(r'^Add PPMP/$', views.addPpmp, name='addPpmp'),
    url(r'^createPPMP$', views.createPPMP, name='createPPMP'),
    url(r'^PPMP List$', views.ppmpList, name='ppmpList'),
    url(r'^PPMP Details/(?P<id>\d+)$', views.ppmpDetailsAdd, name='ppmpDetailsAdd'),
    url(r'^addPPMPDetails/$', views.addPPMPDetails, name='addPPMPDetails'),
    url(r'^Generate PPMP/$', views.ppmpDetailsList, name='ppmpDetailsList'),
    url(r'^Generated PPMP/(?P<id>\d+)$', views.generatedPPMP, name='generatedPPMP'),
    url(r'^Recording In Document/(?P<id>\d+)$', views.viewDocument, name='viewDocument'),
    url(r'^Document List/$', views.listOfDocuments, name='listOfDocuments'),
    url(r'^monitorDoc$', views.monitorDoc, name='monitorDoc'),
    url(r'^monitorDocIn$', views.monitorDocIn, name='monitorDocIn'),
    url(r'^listOfRecordIn$', views.listOfRecordIn, name='listOfRecordIn'),

    # url(r'^sample3/$', views.sample3, name='sample3'),
    # url(r'^api_view/$', views.api_view, name='api_view'),
    # url(r'^time_in/$', views.time_in, name='time_in'),
    

    # url(r'^post/$', views.post, name='post'),

    #API URLS-FOR PPMP AND DOC MON
    url(r'category/', views.category),
    url(r'unit/', views.unit),
    url(r'item/', views.item),
    # url(r'ppmpDetails/', views.ppmpDetails),
    url(r'ItemSearch/', views.CategoryTin.as_view()),
    url(r'^doc/list/$', views.DocumentList.as_view()),
    url(r'^doc/add/$', views.DocumentAdd.as_view()),
    url(r'^doc/search$', views.DocumentSearch.as_view()),
    url(r'^documentMonitoring/$', views.documentMonitoring.as_view()),
    url(r'^docMonitoring/list/$', views.Doc_MonitoringList.as_view()),
    url(r'^docMonitoring/listDetails/$', views.Doc_MonitoringListDetails.as_view()),
    url(r'^docMonitoring/add/$', views.Doc_MonitoringAdd.as_view()),
    url(r'^docMonitoring/edit/$', views.Doc_MonitoringEdit.as_view()),
    url(r'^docMonitoring/search$', views.Doc_MonitoringSearch.as_view()),
    url(r'^ppmp/$', views.ppmp.as_view()),
    url(r'^ppmp/list/$', views.PPMPList.as_view()),
    url(r'^ppmp/listDetails/$', views.PPMPListDetails.as_view()),
    url(r'^ppmp/add/$', views.PPMPAdd.as_view()),
    url(r'^ppmp/edit/$', views.PPMPEdit.as_view()),
    url(r'^ppmpDetails/$', views.ppmpDetails.as_view()),
    url(r'^ppmpDetails/list/$', views.PPMP_DetailsList.as_view()),
    url(r'^ppmpDetails/listDetails/$', views.PPMP_DetailsListDetails.as_view()),
    url(r'^ppmpDetails/add/$', views.PPMP_DetailsAdd.as_view()),
    url(r'^ppmpDetails/edit/$', views.PPMP_DetailsEdit.as_view()),
]

from django.contrib import admin
from .models import Designation, Account, Department, Section, AccSection, Category, Unit, Item, Document, Doc_Monitoring, PPMP, PPMP_Details


admin.site.register(Designation)
admin.site.register(Account)
admin.site.register(Department)
admin.site.register(Section)
admin.site.register(AccSection)
admin.site.register(Category)
admin.site.register(Unit)
admin.site.register(Item)
admin.site.register(Document)
admin.site.register(Doc_Monitoring)
admin.site.register(PPMP)
admin.site.register(PPMP_Details)
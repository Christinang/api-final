$(document).ready(function() {
        var med_data = '';
        $.ajax({
            type: "GET",
            dataType: "JSON",
            url: '/infirmary/patient/',
            success: function(data){
                $.each(data, function(key, value){
                   
                    med_data += '<tr>';
                    med_data += '<td>'+ value.id +'</td>';
                    med_data += '<td>'+ value.pa_surname +'</td>';
                    med_data += '<td>'+ value.pa_fname +'</td>'; 
                    med_data += '<td>'+ value.pa_mname +'</td>';
                    med_data += '<td>'+ value.pa_bdate +'</td>';
                    med_data += '<td>'+ value.pa_civilstat +'</td>'; 
                    med_data += '<td>'+ value.pa_address +'</td>';
                    med_data += '<td>'+ value.pa_religion +'</td>';
                    med_data += '<td>'+ value.pa_guardian +'</td>'; 
                    med_data += '<td>'+ value.pa_relation +'</td>';
                    med_data += '<td>'+ value.pa_contactno +'</td>';
                    med_data += '<td>'+ value.pa_type +'</td>'; 
                    med_data += '</tr>';
                   
                })
                $('#patientTable').append(med_data);
                $('#patientTable').DataTable({
                responsive: true
        });
            },
            error: function (data) {
                alert('error');
            }
        })
        
});


$(document).ready(function() {
        var med_data = '';
        $.ajax({
            type: "GET",
            dataType: "JSON",
            url: '/infirmary/medicine/',
            success: function(data){
                $.each(data, function(key, value){
                   
                    med_data += '<tr>';
                    med_data += '<td>'+ value.id +'</td>';
                    med_data += '<td>'+ value.med_name +'</td>';
                    med_data += '<td>'+ value.med_desc +'</td>'; 
                    med_data += '</tr>';
                   
                })
                $('#dataTables').append(med_data);
                $('#dataTables').DataTable({
                responsive: true
        });
            },
            error: function (data) {
                alert('error');
            }
        })
        
});

$(document).ready(function(){
        $("#myBtn").click(function(){
            $("#myModal").modal();
        });
    });
from django.db import models

# from django.db.models.signals import post_save
# from django.dispatch import receiver
# from rest_framework.authtoken.models import Token
from django.conf import settings
from django_extensions.db.models import TimeStampedModel

# @receiver(post_save, sender=settings.AUTH_USER_MODEL)
# def create_auth_token(sender, instance=None, created=False, **kwaargs):
# 	if created:
# 		Token.objects.create(user=instance)

from django_mysql.models import JSONField

# from django.db.models import CharField
# from django_mysql.models import ListCharField
# from core.models import AccSection
# from msunapi.models import Item

# Create your models here.

class Designation(models.Model):
	name = models.CharField(max_length=50, null=True)
	description = models.CharField(max_length=150, null=True)

	def __str__(self):
		return self.name



class Account(models.Model):
	designation = models.ForeignKey(Designation, on_delete = models.CASCADE, null=True)
	user_id = models.IntegerField(null=True)
	surname = models.CharField(max_length=50, null=True)
	fname = models.CharField(max_length=50, null=True)
	mname = models.CharField(max_length=50, null=True)
	nameExtension = models.CharField(max_length=10, null=True)
	username = models.CharField(max_length=50, null=True)
	password = models.CharField(max_length=50, null=True)
	sex = models.IntegerField(null=True)
	bdate = models.DateField(max_length=20, null=True)
	address = models.CharField(max_length=200, null=True)
	contactno = models.CharField(max_length=200, null=True)
	
	def __str__(self):
		return str(self.fname) + ' ' + str(self.mname) + ' ' + str(self.surname)

class Department(models.Model):
	name = models.CharField(max_length=50, null=True)
	description = models.CharField(max_length=150, null=True)
	mfopapCode = models.CharField(max_length=15, null=True)#
	deptHead = models.CharField(max_length=150, null=True)#
	headPosition = models.CharField(max_length=150, null=True)#


	def __str__(self):
		return self.name


class Section(models.Model):
	department = models.ForeignKey(Department, on_delete = models.CASCADE, null=True)
	name = models.CharField(max_length=50, null=True)
	description = models.CharField(max_length=150, null=True)
	secHead = models.CharField(max_length=150, null=True)#
	headPosition = models.CharField(max_length=150, null=True)#

	def __str__(self):
		return self.name

class AccSection(models.Model):#
	account = models.ForeignKey(Account, on_delete = models.CASCADE, null=True)
	section = models.ForeignKey(Section, on_delete = models.CASCADE, null=True)
	time = models.DateTimeField(auto_now_add=True, null=True)

class Category (TimeStampedModel):
    Description = models.CharField(blank = True, max_length = 75, default = None)

    def __str__ (self):
        return self.Description

class Unit (TimeStampedModel):
    Name = models.CharField(blank = True, max_length = 75, default = None)

    def __str__ (self):
        return self.Name

class Item (TimeStampedModel):
    StockCode = models.CharField(blank = True, max_length = 20, default = None)
    Name = models.CharField(blank = True, max_length = 45, default = None)
    Description = models.CharField(blank = True, max_length = 75, default = None)
    UnitID = models.ForeignKey(Unit, on_delete = models.CASCADE)
    CategoryID = models.ForeignKey(Category, on_delete = models.CASCADE)
    Price = models.FloatField(blank = True, null = True, default = None)

    def __str__ (self):
        return self.StockCode + ' - ' + self.Name

class Document(TimeStampedModel):

	SELECT_DOCUMENTS = 'SELECT_DOCUMENTS'
	PPMP = 'PPMP'
	SPPMP = 'SPPMP'
	PR = 'PR'
	PO = 'PO'
	RIS = 'RIS'
	TYPES = ((SELECT_DOCUMENTS, 'SELECT_DOCUMENTS'), (PPMP, 'PPMP'), (SPPMP, 'SPPMP'), (PR, 'PR'), (PO, 'PO'), (RIS, 'RIS'))

	DocumentType = models.CharField(max_length=10, choices=TYPES, default=SELECT_DOCUMENTS)
	DocumentOwnedBy = models.ForeignKey(AccSection, on_delete=models.CASCADE)

	def __str__(self):
		return self.DocumentType


class PPMP(TimeStampedModel):

	SUPLEMENTALPPMP = 'SUPLEMENTALPPMP'
	ORIGINAL = 'PPMP'
	NULL = 'NULL'
	TYPES = ((ORIGINAL, 'PPMP'), (SUPLEMENTALPPMP, 'SUPLEMENTALPPMP'), (NULL, 'NULL'))

	PpmpCreatedBy = models.ForeignKey(AccSection, on_delete=models.CASCADE)
	ForTheYear = models.CharField(max_length=5, default=None)
	PpmpCode = models.CharField(max_length=20, default=None)
	PpmpType = models.CharField(max_length=20, choices=TYPES, default=ORIGINAL)

	def __str__(self):
		return self.PpmpCode + ' - ' + self.ForTheYear

class PPMP_Details(TimeStampedModel):

	PpmpID = models.ForeignKey(PPMP, on_delete=models.CASCADE, default=None)
	ItemID = models.ForeignKey(Item, on_delete=models.CASCADE, default=None)
	UnitPrice = models.PositiveIntegerField()
	PpmpQuantity = models.PositiveIntegerField()
	EstimatedTotal = models.PositiveIntegerField(default=None)
	Schedule = models.CharField(max_length=1000, default=None)
	# Schedule = JSONField()
	# Schedule = ListCharField(
	# 	base_field=CharField(max_length=100),
	# 	size=30,
	# 	max_length=(30*101)
	# )
# class PR(models.Model):

# 	pr_num = models.PositiveIntegerField()
# 	ppmp = models.ForeignKey(PPMP, on_delete=models.CASCADE)
# 	doc = models.ForeignKey(Document, on_delete=models.CASCADE)
# 	prCreatedOn = models.DateTimeField(auto_now_add=True)

# class PR_Details(models.Model):

# 	pr = models.ForeignKey(PR, on_delete=models.CASCADE)
# 	item = models.ForeignKey(Items, on_delete=models.CASCADE)
# 	pr_quantity = models.PositiveIntegerField()
# 	prDetCreatedOn = models.DateTimeField(auto_now_add=True)

# class PO(models.Model):

# 	po_num = models.PositiveIntegerField()
# 	pr = models.ForeignKey(PR, on_delete=models.CASCADE)
# 	doc = models.ForeignKey(Document, on_delete=models.CASCADE)
# 	poCreatedOn = models.DateTimeField(auto_now_add=True)

# class PO_Details(models.Model):

# 	po = models.ForeignKey(PO, on_delete=models.CASCADE)
# 	item = models.ForeignKey(Items, on_delete=models.CASCADE)
# 	po_quantity = models.PositiveIntegerField()
# 	poDetCreatedOn = models.DateTimeField(auto_now_add=True)

class Doc_Monitoring(TimeStampedModel):

	PENDING = 'PENDING'
	APPROVED = 'APPROVED'
	REJECTED = 'REJECTED'
	STATUS = ((PENDING, 'PENDING'), (APPROVED, 'APPROVED'), (REJECTED, 'REJECTED'))

	PpmpID = models.ForeignKey(PPMP, on_delete=models.CASCADE, default=None)
	MonitoredBy = models.ForeignKey(AccSection, on_delete=models.CASCADE)
	In_Out = models.CharField(max_length=10, default=None)
	DocumentRemarks = models.CharField(max_length=200, default=None)
	DocumentStatus = models.CharField(max_length=15, choices=STATUS, default=PENDING)

	def __str__(self):
		return self.DocumentRemarks
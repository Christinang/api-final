from rest_framework import serializers
from rest_framework.serializers import EmailField, ValidationError, CharField
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.shortcuts import render, HttpResponse
from .models import Designation, Account, Section, Department, AccSection, Category, Unit, Item, Document, Doc_Monitoring, PPMP, PPMP_Details
# from drf_writable_nested import WritableNestedModelSerializer


class CategorySerializer (serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'Description')

class UnitSerializer (serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields = ('id', 'Name')

class Item_GETSerializer (serializers.ModelSerializer):
    UnitID = UnitSerializer()
    CategoryID = CategorySerializer()
    class Meta:
        model = Item
        fields = ('id','StockCode', 'Name', 'Description', 'UnitID', 'CategoryID', 'Price')

class ItemSerializer (serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('__all__')

# 	def create(self, validated_data):
#         m = Methods()
#         prefix = m.cleanString(validated_data['Name'])
#         no = Item.objects.filter(StockCode__icontains = prefix).count()+1
#         validated_data['StockCode'] = prefix + '-' + str(no)
#         return Item.objects.create(**validated_data)

# class Methods:
#     def cleanString(self, n):
#     	if ((n != None) | (n != '')):
#     		if (len(n) <= 4):
#     			return n.upper()

#     		else:
#     			vowels = ('a', 'e', 'i', 'o', 'u')
#     			name = n.lower()
#     			name = ''.join([char for char in name if char not in vowels])
#     			name = ''.join(name.split())

#     			if (len(name) < 3):
#     				name = ''.join(n.split())

#     			name = name[0 : min(4, len(n)-1)]
#     			name = name.upper()
#     			return name
#     	return None

class DesignationSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	class Meta:
		model = Designation
		fields = [
			'id',
			'name',
			'description',
			'result',
		]


class AccountSerializer(serializers.ModelSerializer):
	result = CharField(allow_blank=True, read_only=True)
	designation = DesignationSerializer()
	class Meta:
		model = Account
		fields = [
			'designation',
			'surname',
			'fname',
			'mname',
			'nameExtension',
			'result',
		]
class DepartmentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Department
		fields = ('name',)


class SectionSerializer(serializers.ModelSerializer):
	department = DepartmentSerializer()
	class Meta:
		model = Section
		fields = ['name','department']


class AccSectionSerializer(serializers.ModelSerializer):
	account = AccountSerializer()
	section = SectionSerializer()
	class Meta:
		model = AccSection
		fields = [
			'account',
			'section',
			'time',
		]

class DocumentTypeSerializer(serializers.ModelSerializer):
	DocumentOwnedBy = AccSectionSerializer()
	class Meta:
		model = Document
		fields = ('DocumentType', 'DocumentOwnedBy')

class DocumentListSerializer(serializers.ModelSerializer):

	class Meta:
		model = Document
		fields = ('id', 'DocumentType',)

class DocumentAddSerializer(serializers.ModelSerializer):

	class Meta:
		model = Document
		fields = ('__all__')

# E balik rani nako nga validation pag naa nakoy sa account manager nga api

	# def validate(self, request):

	# 	try:
	# 		Document.objects.get(doc_type=request['doc_type'])
	# 	except Document.DoesNotExist:
	# 		pass
	# 	except Document.MultipleObjectsReturned:
	# 		raise serializers.ValidationError('Document Already Exist')
	# 	else:
	# 		raise serializers.ValidationError('Document Already Exist')
	# 	return request

class DocumentEditSerializer(serializers.ModelSerializer):

	class Meta:
		model = Document
		fields = ('__all__')

	def validate(self, request):

		try:
			Document.objects.get(DocumentType=request['DocumentType'])
		except Document.DoesNotExist:
			pass
		except Document.MultipleObjectsReturned:
			raise serializers.ValidationError('Document Already Exist')
		else:
			raise serializers.ValidationError('No Changes')
		return request


class PPMPListSerializer(serializers.ModelSerializer):
	PpmpCreatedBy = AccSectionSerializer()
	class Meta:
		model = PPMP
		fields = ('__all__')

# 	def create(self, validated_data):
#         m = Methods()
#         prefix = m.cleanString(validated_data['PpmpType'])
#         no = PPMP.objects.filter(PpmpCode__icontains = prefix).count()+1
#         validated_data['PpmpCode'] = prefix + '-' + str(no)
#         return PPMP.objects.create(**validated_data)

# class Methods:
#     def cleanString(self, n):
#     	if ((n != None) | (n != '')):
#     		if (len(n) <= 4):
#     			return n.upper()

#     		else:
#     			vowels = ('a', 'e', 'i', 'o', 'u')
#     			name = n.lower()
#     			name = ''.join([char for char in name if char not in vowels])
#     			name = ''.join(name.split())

#     			if (len(name) < 3):
#     				name = ''.join(n.split())

#     			name = name[0 : min(4, len(n)-1)]
#     			name = name.upper()
#     			return name
#     	return None

class PPMPListSerializer1(serializers.ModelSerializer):
	class Meta:
		model = PPMP
		fields = '__all__'

class PPMPCodeSerializer(serializers.ModelSerializer):

	class Meta:
		model = PPMP
		fields = ('PpmpCode','DocumentID',)

class PPMPAddSerializer(serializers.ModelSerializer):
	# doc = DocumentTypeSerializer()

	class Meta:
		model = PPMP
		fields = ['id', 'ForTheYear', 'PpmpCode', 'PpmpType', 'PpmpCreatedBy',]

	# def create(self, validated_data):
	# 	doc_data = validated_data.pop('doc')
	# 	ppmp = PPMP.objects.create(**validated_data)
	# 	Document.objects.create(ppmp=ppmp, **doc_data)
	# 	return ppmp	

	# def to_representation(self, instance):
	# 	representation = super(PPMPAddSerializer, self).to_representation(instance)
	# 	representation['doc'] = DocumentListSerializer(instance.doc_set.all())
	# 	return representation

	# def validate(self, request):

	# 	PpmpCode = request.get("PpmpCode", None)

	# 	try:
	# 		PPMP.objects.get(PpmpCode=request['PpmpCode'])
	# 	except PPMP.DoesNotExist:
	# 		request['PpmpCode'] = PpmpCode
	# 	except PPMP.MultipleObjectsReturned:
	# 		raise serializers.ValidationError('Plan Already Exist')
	# 	else:
	# 		raise serializers.ValidationError('Plan Already Exist')
	# 	return request

class PPMPEditSerializer(serializers.ModelSerializer):

	class Meta:
		model = PPMP
		fields = ('__all__')

	def validate(self, request):
		
		ForTheYear = request.get("ForTheYear", None)
		PpmpCode = request.get("PpmpCode", None)
		PpmpType = request.get("PpmpType", None)
		DocumentID = request.get("DocumentID", None)

		try:
			PPMP.objects.get(ForTheYear=request['ForTheYear'],PpmpCode=request['PpmpCode'],PpmpType=request['PpmpType'],DocumentID=request['DocumentID'])
		except PPMP.DoesNotExist:
			request['ForTheYear'] = ForTheYear
			request['PpmpCode'] = PpmpCode
			request['PpmpType'] = PpmpType
			request['DocumentID'] = DocumentID
		except PPMP.MultipleObjectsReturned:
			raise serializers.ValidationError('Plan Already Exist')
		else:
			raise serializers.ValidationError('No Changes')
		return request

class PPMP_DetailsListSerializer(serializers.ModelSerializer):
	PpmpID = PPMPListSerializer()
	ItemID = Item_GETSerializer()
	class Meta:
		model = PPMP_Details
		fields = ('PpmpID', 'ItemID', 'UnitPrice', 'PpmpQuantity', 'EstimatedTotal', 'Schedule',)

class PPMP_DetailsAddSerializer(serializers.ModelSerializer):


	class Meta:
		model = PPMP_Details
		fields = '__all__'

	# def validate(self, request):

	# 	PpmpID = request.get("PpmpID", None)
	# 	ItemID = request.get("ItemID", None)

	# 	try:
	# 		ppmp_details = PPMP_Details.objects.get(PpmpID=request['PpmpID'], ItemID=request['ItemID'])
	# 	except PPMP_Details.DoesNotExist:
	# 		request['PpmpID'] = PpmpID
	# 		request['ItemID'] = ItemID
	# 	except PPMP_Details.MultipleObjectsReturned:
	# 		raise serializers.ValidationError('Plan Details Already Exist')
	# 	else:
	# 		raise serializers.ValidationError('Plan Details Already Exist')
	# 	return request

class PPMP_DetailsEditSerializer(serializers.ModelSerializer):

	class Meta:
		model = PPMP_Details
		fields = ('__all__')

	def validate(self, request):

		PpmpID = request.get("PpmpID", None)
		ItemID = request.get("ItemID", None)
		UnitPrice = request.get("UnitPrice", None)
		PpmpQuantity = request.get("PpmpQuantity", None)
		Schedule = request.get("Schedule", None)

		try:
			PPMP_Details.objects.get(PpmpID=request['PpmpID'],ItemID=request['ItemID'],UnitPrice=request['UnitPrice'],PpmpQuantity=request['PpmpQuantity'],Schedule=request['Schedule'])
		except PPMP_Details.DoesNotExist:
			request['PpmpID'] = PpmpID
			request['ItemID'] = ItemID
			request['UnitPrice'] = UnitPrice
			request['Schedule'] = Schedule
		except PPMP_Details.MultipleObjectsReturned:
			raise serializers.ValidationError('Plan Details Already Exist')
		else:
			raise serializers.ValidationError('No Changes')
		return request

class Doc_MonitoringListSerializer(serializers.ModelSerializer):
	PpmpID = PPMPListSerializer()
	MonitoredBy = AccSectionSerializer()
	class Meta:
		model = Doc_Monitoring
		fields = ('__all__')

class Doc_MonitoringAddSerializer(serializers.ModelSerializer):

	class Meta:
		model = Doc_Monitoring
		fields = ('__all__')

	# def validate(self, request):
	# 	try:
	# 		Doc_Monitoring.objects.get(PpmpDetails=request['PpmpDetails'],In_Out=request['In_Out'],DocumentRemarks=request['DocumentRemarks'])
	# 	except Doc_Monitoring.DoesNotExist:
	# 		pass
	# 	except Doc_Monitoring.MultipleObjectsReturned:
	# 		raise serializers.ValidationError('Document Already Monitored')
	# 	else:
	# 		raise serializers.ValidationError('Document Already Monitored')
	# 	return request

class Doc_MonitoringEditSerializer(serializers.ModelSerializer):

	class Meta:
		model = Doc_Monitoring
		fields = ('__all__')

	def validate(self, request):
		try:
			Doc_Monitoring.objects.get(PpmpDetails=request['PpmpDetails'],In_Out=request['In_Out'],DocumentRemarks=request['DocumentRemarks'])
		except Doc_Monitoring.DoesNotExist:
			pass
		except Doc_Monitoring.MultipleObjectsReturned:
			raise serializers.ValidationError('Document Already Monitored')
		else:
			raise serializers.ValidationError('Document Already Monitored')
		return request
#KANG CHRISTINE NGA MGA IMPORTS
from django.shortcuts import get_object_or_404

from django.db.models import Q
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from django.http import JsonResponse
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.template import loader
from django.template.loader import render_to_string
from rest_framework import parsers, renderers, generics, status
from rest_framework import mixins
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer

# from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
# from rest_framework.authentication import TokenAuthentication

from rest_framework.renderers import TemplateHTMLRenderer
from .models import Category, Unit, Item, Document, Doc_Monitoring, PPMP, PPMP_Details, AccSection
from .serializers import CategorySerializer, UnitSerializer, Item_GETSerializer, ItemSerializer, DocumentListSerializer, DocumentAddSerializer, DocumentEditSerializer, Doc_MonitoringListSerializer, Doc_MonitoringAddSerializer, Doc_MonitoringEditSerializer, PPMPListSerializer, PPMPListSerializer1, PPMPAddSerializer, PPMPEditSerializer, PPMP_DetailsListSerializer, PPMP_DetailsAddSerializer, PPMP_DetailsEditSerializer
from supportOffice import serializers
#DIRI TAMAN
from django.shortcuts import render, redirect, HttpResponse
from django import template
from django.conf.urls import url
import requests, json
import datetime
from datetime import datetime, timezone
from time import gmtime, strftime
# def getHostNameInfo():
#     now = datetime.now()
#     print(now.strftime("%A, %B %d, %Y, %I:%M:%S%p"))
# getHostNameInfo()
def index(request):
    return render(request, 'supportOffice/index.html',)

def home(request):
    return render(request, 'supportOffice/home.html',)

def addPpmp(request):
    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/')
    addPpmp=json.loads(req.content.decode('UTF-8'))
    return render(request, 'supportOffice/addPpmp.html', {'addPpmp': addPpmp })

def createPPMP(request):

    data = {
        "ForTheYear":request.GET['ppmp_year'],
        "PpmpCode":request.GET['ppmp_code'],
        "PpmpType":request.GET['ppmp_type'],
        "PpmpCreatedBy":request.GET['createdBy']
    }


    temp=json.dumps(data)
    #return HttpResponse(temp, content_type="application/json; charset=UTF-8")
    req=requests.post('http://127.0.0.1:8000/supportOffice/ppmp/', temp, headers={'content-type':'application/json'} )
    return HttpResponse(req.content,content_type="application/json")
    #requests.post('http://127.0.0.1:8000/supportOffice/diagnosis/',temp2, headers={'content-type':'application/json'} )
    #return redirect('supportOffice:add_consultation')

def ppmpList(request):    

    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/')
    ppmpList=json.loads(req.content.decode('UTF-8'))
    return render(request, 'supportOffice/ppmpList.html', {'ppmpList': ppmpList})

def ppmpDetailsAdd(request, id):

    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/?id='+id)
    ppmp=json.loads(req.content.decode('UTF-8'))

    reqq=requests.get('http://127.0.0.1:8000/supportOffice/ppmpDetails/')
    ppmpDet=json.loads(reqq.content.decode('UTF-8'))

    reqqq=requests.get('http://127.0.0.1:8000/supportOffice/item/')
    item=json.loads(reqqq.content.decode('UTF-8'))
    return render(request, 'supportOffice/ppmpDetails.html', {'ppmp': ppmp, 'ppmpDet': ppmpDet, 'item': item})
    
def addPPMPDetails(request):

    data = {
        "PpmpID":request.GET['ppmpID'],
        "UnitPrice":request.GET['ppmpDetPrice'],
        "PpmpQuantity":request.GET['ppmpDetQty'],
        "Schedule":request.GET['ppmpDetSched'],
        "ItemID":request.GET['ppmpDetItem'],
        "EstimatedTotal":request.GET['ppmpEsTotal'],
    }

    temp=json.dumps(data)
    #return HttpResponse(temp, content_type="application/json; charset=UTF-8")
    req=requests.post('http://127.0.0.1:8000/supportOffice/ppmpDetails/', temp, headers={'content-type':'application/json'} )
    return HttpResponse(req.content,content_type="application/json")
    #requests.post('http://127.0.0.1:8000/supportOffice/diagnosis/',temp2, headers={'content-type':'application/json'} )
    #return redirect('supportOffice:add_consultation')

def ppmpDetailsList(request):

    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/')
    ppmpList=json.loads(req.content.decode('UTF-8'))
    return render(request, 'supportOffice/ppmpDetailsList.html', {'ppmpList': ppmpList})

def listOfDocuments(request):

    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/')
    ppmpList=json.loads(req.content.decode('UTF-8'))
    return render(request, 'supportOffice/listOfDocuments.html', {'ppmpList': ppmpList})

def generatedPPMP(request, id):

    reqq=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/?id='+id)
    ppmp=json.loads(reqq.content.decode('UTF-8'))

    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmpDetails/?id='+id)
    ppmpList=json.loads(req.content.decode('UTF-8'))

    return render(request, 'supportOffice/generatedPPMP.html', {'ppmpList': ppmpList, 'ppmp': ppmp})

def viewDocument(request, id):

    reqq=requests.get('http://127.0.0.1:8000/supportOffice/ppmp/?id='+id)
    ppmp=json.loads(reqq.content.decode('UTF-8'))

    req=requests.get('http://127.0.0.1:8000/supportOffice/ppmpDetails/?id='+id)
    ppmpList=json.loads(req.content.decode('UTF-8'))

    return render(request, 'supportOffice/viewDocument.html', {'ppmpList': ppmpList, 'ppmp': ppmp})

def monitorDoc(request):

    data = {
        "PpmpID": request.GET['ppmp_id'],
        "In_Out":request.GET['document_in_out'],
        "DocumentStatus": request.GET['document_status'],
        "DocumentRemarks":request.GET['document_remarks'],
        "MonitoredBy":request.GET['monitored_by'],
    }

    temp=json.dumps(data)
    #return HttpResponse(temp, content_type="application/json; charset=UTF-8")
    req=requests.post('http://127.0.0.1:8000/supportOffice/documentMonitoring/',temp, headers={'content-type':'application/json'} )
    return HttpResponse(req.content,content_type="application/json")

def monitorDocIn(request):

    data = {
        "PpmpID": request.GET['ppmp_id1'],
        "In_Out":request.GET['document_in_out1'],
        "DocumentStatus": request.GET['document_status1'],
        "DocumentRemarks":request.GET['document_remarks1'],
        "MonitoredBy":request.GET['monitored_by1'],
    }

    temp=json.dumps(data)
    #return HttpResponse(temp, content_type="application/json; charset=UTF-8")
    req=requests.post('http://127.0.0.1:8000/supportOffice/docMonitoring/add/',temp, headers={'content-type':'application/json'} )
    return HttpResponse(req.content,content_type="application/json")

def listOfRecordIn(request):

    reqq=requests.get('http://127.0.0.1:8000/supportOffice/docMonitoring/list/')
    ppmp=json.loads(reqq.content.decode('UTF-8'))

    return render(request, 'supportOffice/listOfRecordIn.html', {'ppmp': ppmp})


# Use to access api Search bar RECORD OUT
# def sample3(request):
#     req = requests.get('http://127.0.0.1:8000/supportOffice/docMonitoring/search?q=' + request.GET['query'])
#     return HttpResponse(req.content, content_type = "application/json; charset=UTF-8")

# def api_view(request):
#     req = requests.get('http://127.0.0.1:8000/supportOffice/ppmpDetails/listDetails/?id=' + request.GET['query'])
#     return HttpResponse(req.content, content_type = "application/json; charset=UTF-8")

# def time_in(request):
#     req = requests.get('http://127.0.0.1:8000/supportOffice/ppmpDetails/listDetails/?id=' + request.GET['query'])
#     return HttpResponse(req.content, content_type = "application/json; charset=UTF-8")


# def post(self, request, format=None):
#     time = request.data['time']
#     section = request.data['section']


#     if not time and not section:
#         raise Http404
#     else:
#         designation = designation.objects.get(name=name)
#         designation = designation.objects.get(time=time)
#         designation.name=name
#         designation.time=time
#         designation.save()

   # req = requests.get('http://127.0.0.1:8000/supportOffice/docMonitoring/search?q=' + request.GET['query'])

#    now = datetime.now()
#     print(now.strftime("%A, %B %d, %Y, %I:%M:%S%p")
#     api_record_in()
#    return HttpResponse(req.content, content_type = "application/json; charset=UTF-8")


# KANG CHRISTINE NGA MGA VIEWS
@csrf_exempt
@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def category(request):
    if request.method == 'GET':
        if 'id' in request.GET:
            id = request.GET['id']
            category = Category.objects.get(pk=id)
            serializer = CategorySerializer(category)
            return JsonResponse(serializer.data, safe = False)
        elif 'Description' in request.GET:
            Description = request.GET['Description']
            category = Category.objects.filter(Description__icontains = Description)
            serializer = CategorySerializer(category, many = True)
            return JsonResponse(serializer.data, safe = False)
        else:
            category = Category.objects.all()
            serializer = CategorySerializer(category, many = True)
            return JsonResponse(serializer.data, safe = False)

    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CategorySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status = 201)
        return JsonResponse(serializer.errors, status = 400)


    elif request.method == 'PUT':
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                category = Category.objects.get(pk=id)
                data = JSONParser().parse(request)
                serializer = CategorySerializer(category, data=data)
                if serializer.is_valid():
                    serializer.save()
                    return JsonResponse(serializer.data)
                return JsonResponse(serializer.errors, status = 400)
            except Category.DoesNotExist:
                return HttpResponse(status = 404)

    elif request.method == 'DELETE':
        id = request.GET['id']
        try:
            category = Category.objects.get(pk=id)
            category.delete()
            return HttpResponse(status = 204)
        except Category.DoesNotExist:
            return HttpResponse(status = 404)

@csrf_exempt
@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def unit(request):
    if request.method == 'GET':
        if 'id' in request.GET:
            id = request.GET['id']
            unit = Unit.objects.get(pk=id)
            serializer = UnitSerializer(unit)
            return JsonResponse(serializer.data, safe = False)
        elif 'Name' in request.GET:
            Name = request.GET['Name']
            unit = Unit.objects.filter(Name__icontains = Name)
            serializer = UnitSerializer(unit, many = True)
            return JsonResponse(serializer.data, safe = False)
        else:
            unit = Unit.objects.all()
            serializer = UnitSerializer(unit, many = True)
            return JsonResponse(serializer.data, safe = False)

    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = UnitSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status = 201)
        return JsonResponse(serializer.errors, status = 400)


    elif request.method == 'PUT':
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                unit = Unit.objects.get(pk=id)
                data = JSONParser().parse(request)
                serializer = UnitSerializer(unit, data=data)
                if serializer.is_valid():
                    serializer.save()
                    return JsonResponse(serializer.data)
                return JsonResponse(serializer.errors, status = 400)
            except Unit.DoesNotExist:
                return HttpResponse(status = 404)

    elif request.method == 'DELETE':
        id = request.GET['id']
        try:
            unit = Unit.objects.get(pk=id)
            unit.delete()
            return HttpResponse(status = 204)
        except Unit.DoesNotExist:
            return HttpResponse(status = 404)

@csrf_exempt
@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def item(request):
    if request.method == 'GET':
        if 'id' in request.GET:
            id = request.GET['id']
            item = Item.objects.get(pk=id)
            serializer = Item_GETSerializer(item)
            return JsonResponse(serializer.data, safe = False)
        elif 'StockCode' in request.GET:
            stockCode = request.GET['stockCode']
            item = Item.objects.filter(StockCode__icontains = stockCode)
            serializer = Item_GETSerializer(item, many = True)
            return JsonResponse(serializer.data, safe = False)
        elif 'Name' in request.GET:
            name = request.GET['name']
            item = Item.objects.filter(Name__icontains = name)
            serializer = Item_GETSerializer(item, many = True)
            return JsonResponse(serializer.data, safe = False)
        elif 'Description' in request.GET:
            Description = request.GET['Description']
            item = Item.objects.filter(Description__icontains = Description)
            serializer = Item_GETSerializer(item, many = True)
            return JsonResponse(serializer.data, safe = False)
        elif 'CategoryID' in request.GET:
            CategoryID = request.GET['CategoryID']
            item = Item.objects.filter(CategoryID = CategoryID)
            serializer = Item_GETSerializer(item, many = True)
            return JsonResponse(serializer.data, safe = False)
        else:
            item = Item.objects.all()
            serializer = Item_GETSerializer(item, many = True)
            return JsonResponse(serializer.data, safe = False)

    if request.method == 'POST':
        data = JSONParser().parse(request)
        prefix = cleanString(data['Name'])
        no = Item.objects.filter(StockCode__icontains = prefix).count()+1
        data['StockCode'] = prefix + '-' + str(no)
        serializer = ItemSerializer(data = data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status = 201)
        return JsonResponse(serializer.errors, status = 400)


    elif request.method == 'PUT':
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                item = Item.objects.get(pk=id)
                data = JSONParser().parse(request)
                serializer = ItemSerializer(item, data=data)
                if serializer.is_valid():
                    serializer.save()
                    return JsonResponse(serializer.data)
                return JsonResponse(serializer.errors, status = 400)
            except Item.DoesNotExist:
                return HttpResponse(status = 404)

    elif request.method == 'DELETE':
        id = request.GET['id']
        try:
            item = Item.objects.get(pk=id)
            item.delete()
            return HttpResponse(status = 204)
        except Item.DoesNotExist:
            return HttpResponse(status = 404)

# @csrf_exempt
# @api_view(['GET', 'POST', 'PUT', 'DELETE'])
# def ppmpDetails(request):
#     if request.method == 'GET':
#         if 'id' in request.GET:
#             id = request.GET['id']
#             ppmpDetID = PPMP_Details.objects.get(pk=id)
#             serializer = PPMP_DetailsListSerializer(ppmpDetID)
#             return JsonResponse(serializer.data, safe = False)
#         elif 'PpmpID' in request.GET:
#             stockCode = request.GET['PpmpID']
#             item = PPMP_Details.objects.filter(PpmpID = stockCode)
#             serializer = PPMP_DetailsListSerializer(item, many = True)
#             return JsonResponse(serializer.data, safe = False)
#         else:
#             item = PPMP_Details.objects.all()
#             serializer = PPMP_DetailsListSerializer(item, many = True)
#             return JsonResponse(serializer.data, safe = False)

#     if request.method == 'POST':
#         data = JSONParser().parse(request)
#         serializer = PPMPAddSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status = 201)
#         return JsonResponse(serializer.errors, status = 400)

#     elif request.method == 'PUT':
#         if 'id' in request.GET:
#             try:
#                 id = request.GET['id']
#                 item = PPMP_Details.objects.get(pk=id)
#                 data = JSONParser().parse(request)
#                 serializer = PPMP_DetailsEditSerializer(item, data=data)
#                 if serializer.is_valid():
#                     serializer.save()
#                     return JsonResponse(serializer.data)
#                 return JsonResponse(serializer.errors, status = 400)
#             except PPMP_Details.DoesNotExist:
#                 return HttpResponse(status = 404)

#     elif request.method == 'DELETE':
#         id = request.GET['id']
#         try:
#             item = PPMP_Details.objects.get(pk=id)
#             item.delete()
#             return HttpResponse(status = 204)
#         except PPMP_Details.DoesNotExist:
#             return HttpResponse(status = 404)

class CategoryTin(ListAPIView):
    serializer_class = CategorySerializer
    filter_backends = [SearchFilter]
    search_fields = ['CategoryID']

    def get_queryset(self, *args, **kwargs):
        queryset_list = Item.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.select_related(
                Q(cat_description=query)
                ).distinct()
        return queryset_list



class DocumentSearch(APIView):

    def get_object(self, name):
        try:
            return Document.objects.get(type=type)
        except Document.DoesNotExist:
            raise Http404

    def get(self, request, name, format=None):
        document = self.get_object(type)
        serializer = DocumentSerializer(document)
        return Response(serializer.data)

class DocumentList(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = DocumentListSerializer

    def get(self, request, format=None):
        document = Document.objects.all()
        serializer = DocumentListSerializer(document, many=True)
        return Response(serializer.data)




class DocumentAdd(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = DocumentAddSerializer

    def post(self, request, format=None):
        serializer = DocumentAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DocumentEdit(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = DocumentEditSerializer

    def put(self, request, format=None):
        id = request.GET['id']
        document = Document.objects.get(pk=id)
        serializer = DocumentEditSerializer(document, data=request.data)
        if serializer.is_valid():
            serializer.save()
        else:
            result = "Document Already Exist"
            return Response(result)


class DocumentSearch(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        q = request.GET['q']
        if q:           
            try:
                document = Doc_Monitoring.objects.get(DocumentID=q)
            except Doc_Monitoring.DoesNotExist:
                raise Http404

            serializer = Doc_MonitoringListSerializer(document)
            return Response(serializer.data)
        else:
            return null

# class PPMPList(APIView):

#     # authentication_class = (TokenAuthentication,)
#     # permission_classes = (IsAuthenticated,)

#     serializer_class = PPMPListSerializer1

#     def get(self, request, format=None):
#         ppmp = PPMP.objects.all().prefetch_related('DocumentID')
#         # ppmp = PPMP.objects.all()
#         serializer = PPMPListSerializer1(ppmp, many=True)
#         return Response(serializer.data)

class ppmp(APIView):
    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                ppmp = PPMP.objects.get(pk=id)
                serializer = PPMPListSerializer(ppmp)
                return Response(serializer.data)
            except PPMP.DoesNotExist:
                result = "supportOffice Does Not Exist!"
                return(result)
        else:
            ppmp = PPMP.objects.all()
            serializer = PPMPListSerializer(ppmp, many=True)
            return Response(serializer.data)

    def post(self, request):                
        serializer = PPMPListSerializer1(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "PPMP Already Exist!"
            return Response(result)

    def put(self, request):
        id = request.GET['id']
        ppmp = PPMP.objects.get(pk=id)
        serializer = PPMPEditSerializer(ppmp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "No Changes"
            return Response(result)

class ppmpDetails(APIView):

    def get(self, request, format=None):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                Ppmp = PPMP.objects.get(pk=id)
                ppmpDetails = PPMP_Details.objects.all()._next_is_sticky().filter(PpmpID=Ppmp)
                ppmpDetails = PPMP_DetailsListSerializer(ppmpDetails, many=True)
                return Response(ppmpDetails.data)
            except PPMP_Details.DoesNotExist:
                result = "supportOffice Does Not Exist!"
                return(result)
        else:
            ppmpDetails = PPMP_Details.objects.all()
            serializer = PPMP_DetailsListSerializer(ppmpDetails, many=True)
            return Response(serializer.data)

    def post(self, request):
        ppmpdetails = PPMP_DetailsAddSerializer(data=request.data)
        if ppmpdetails.is_valid():
            ppmpdetails.save()
            return Response(ppmpdetails.data)
        else:
            result = "PPMP Details Already Exist!"
            return Response(result)

    def put(self, request, format=None):
        id = request.GET['id']
        ppmp_details = PPMP_Details.objects.get(pk=id)
        serializer = PPMP_DetailsEditSerializer(ppmp_details, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "No Changes"
            return Response(result)

class documentMonitoring(APIView):

    def get(self, request):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                doc_monitoring = Doc_Monitoring.objects.get(pk=id)
                serializer = Doc_MonitoringListSerializer(doc_monitoring)
                return Response(serializer.data)
            except Doc_Monitoring.DoesNotExist:
                result = "The document does not exist it may not be yet approved."
                return(result)
        else:
            doc_monitoring = Doc_Monitoring.objects.all()
            serializer = Doc_MonitoringListSerializer(doc_monitoring, many=True)
            return Response(serializer.data)

    def post(self, request):
        serializer = Doc_MonitoringAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "Document Already Monitored"
            return Response(result)

    def put(self, request, format=None):
        id = request.GET['id']
        doc_monitoring = Doc_Monitoring.objects.get(pk=id)
        serializer = Doc_MonitoringEditSerializer(doc_monitoring, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "Document Already Monitored"
            return Response(result)

class PPMPList(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMPListSerializer1

    def get(self, request, format=None):
        ppmp = PPMP.objects.all()
        # ppmp = PPMP.objects.all()
        serializer = PPMPListSerializer1(ppmp, many=True)
        return Response(serializer.data)

class PPMPListDetails(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMPListSerializer

    def get(self, request, format=None):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                ppmp = PPMP.objects.get(pk=id)
                serializer = PPMPListSerializer(ppmp)
                return Response(serializer.data)
            except PPMP.DoesNotExist:
                result = "supportOffice Does Not Exist!"
                return(result)
        else:
            ppmp = PPMP.objects.all()
            serializer = PPMPListSerializer(ppmp, many=True)
            return Response(serializer.data)

class PPMPAdd(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMPAddSerializer

    def post(self, request):

        # serializer = PPMPAddSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data)
        # else:
        #     result = "PPMP Already Exist!"
        #     return Response(result)

        ForTheYear = request.data['ForTheYear']
        # user_id
        PpmpCode = request.data['PpmpCode']
        PpmpType = request.data['PpmpType']
        PpmpCreatedBy = request.data['PpmpCreatedBy']   
        if not ForTheYear and not PpmpCode and not PpmpType and not PpmpCreatedBy:
            raise Http404
        else:
            try:
                ppmp = PPMP.objects.get(id=id)
            except PPMP.DoesNotExist:
                ppmp = PPMP()
                ppmp.ForTheYear = ForTheYear
                ppmp.PpmpCode = PpmpCode
                ppmp.PpmpType = PpmpType
                ppmp.PpmpCreatedBy = PpmpCreatedBy
                ppmp.result = '1'
                ppmp.save()
                serializer = PPMPAddSerializer(ppmp)
                return Response(serializer.data)
            else:
                ppmp = PPMP()
                ppmp.ForTheYear = ForTheYear
                ppmp.PpmpCode = PpmpCode
                ppmp.PpmpType = PpmpType
                ppmp.PpmpCreatedBy = PpmpCreatedBy
                ppmp.result = 'Already Exists'
                serializer = PPMPAddSerializer(ppmp)
                return Response(serializer.data)

class PPMPEdit(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMPEditSerializer

    def put(self, request, format=None):
        id = request.GET['id']
        ppmp = PPMP.objects.get(pk=id)
        serializer = PPMPEditSerializer(ppmp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "No Changes"
            return Response(result)

class PPMP_DetailsList(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMP_DetailsListSerializer

    def get(self, request, format=None):
        ppmp_details = PPMP_Details.objects.all()
        serializer = PPMP_DetailsListSerializer(ppmp_details, many=True)
        return Response(serializer.data)

class PPMP_DetailsListDetails(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMP_DetailsListSerializer

    def get(self, request, format=None):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                ppmpDetails = PPMP_Details.objects.get(pk=id)
                serializer = PPMP_DetailsListSerializer(ppmpDetails)
                return Response(serializer.data)
            except PPMP.DoesNotExist:
                result = "supportOffice Does Not Exist!"
                return(result)
        else:
            ppmpDetails = PPMP_Details.objects.all()
            serializer = PPMP_DetailsListSerializer(ppmpDetails, many=True)
            return Response(serializer.data)

class PPMP_DetailsAdd(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = PPMP_DetailsAddSerializer

    def post(self, request, format=None):
        serializer = PPMP_DetailsAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "PPMP Details Already Exist!"
            return Response(result)

    def put(self, request, format=None):
        id = request.GET['id']
        ppmp_details = PPMP_Details.objects.get(pk=id)
        serializer = PPMP_DetailsEditSerializer(ppmp_details, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "No Changes"
            return Response(result)

class PPMP_DetailsEdit(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    
    serializer_class = PPMP_DetailsEditSerializer

    def put(self, request, format=None):
        id = request.GET['id']
        ppmp_details = PPMP_Details.objects.get(pk=id)
        serializer = PPMP_DetailsEditSerializer(ppmp_details, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "No Changes"
            return Response(result)

class Doc_MonitoringList(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = Doc_MonitoringListSerializer

    def get(self, request, format=None):
        doc_monitoring = Doc_Monitoring.objects.all()
        serializer = Doc_MonitoringListSerializer(doc_monitoring, many=True)
        return Response(serializer.data)

class Doc_MonitoringListDetails(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = Doc_MonitoringListSerializer

    def get(self, request):
        if 'id' in request.GET:
            try:
                id = request.GET['id']
                doc_monitoring = Doc_Monitoring.objects.get(pk=id)
                serializer = Doc_MonitoringListSerializer(doc_monitoring)
                return Response(serializer.data)
            except Doc_Monitoring.DoesNotExist:
                result = "The document does not exist it may not be yet approved."
                return(result)
        else:
            doc_monitoring = Doc_Monitoring.objects.all()
            serializer = Doc_MonitoringListSerializer(doc_monitoring, many=True)
            return Response(serializer.data)

class Doc_MonitoringAdd(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = Doc_MonitoringAddSerializer

    def post(self, request, format=None):
        serializer = Doc_MonitoringAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            result = "Document Already Monitored"
            return Response(result)

class Doc_MonitoringEdit(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    serializer_class = Doc_MonitoringEditSerializer

    def put(self, request, format=None):
        id = request.GET['id']
        doc_monitoring = Doc_Monitoring.objects.get(pk=id)
        serializer = Doc_MonitoringEditSerializer(doc_monitoring, data=request.data)
        if serializer.is_valid():
            serializer.save()
        else:
            result = "Document Already Monitored"
            return Response(result)

class Doc_MonitoringSearch(APIView):

    # authentication_class = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        q = request.GET['q']
        if q:           
            try:
                ppmp = PPMP_Details.objects.get(PpmpID=q)
            except PPMP.DoesNotExist:
                raise Http404

            serializer = PPMP_DetailsListSerializer(ppmp)
            return Response(serializer.data)
        else:
            return null